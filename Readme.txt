Steps to run uBoost tutorial:

0) This tutorial assumes you ROOT built on your machine, and modification to TMVA was developed and tested using ROOT version 5.34.09

1) Compile local TMVA version by

cd TMVA-v4.1.2/
make

2) Set environment variable TMVALOC to point to your newly compiled version TMVA-v4.1.2, e.g., export TMVALOC=/some/path/TMVA-v4.1.2  

3) Run the example macros: first uBoostTrain.C then uBoostEval.C.   N.b., our test generates a very high-stats Dalitz efficiency plot.  This takes a while since there are 400k events.  
4) To reproduce some plots from the paper run the macros in this sequence:
uBoostTrain.C
uBoostEval.C
uBoostPlots.C
paperPlots.C

The current options availabe for uBDT can be found in uBoostTrain.C where the most relevant is:

UBDT_Num=100           // Number efficiency steps used in UBDT
UBDT_MinEffic=0.0      // Minimum target efficiency for training steps
UBDT_MaxEffic=1.0      // Maximum target efficiency for training steps

If you have questions or comments contact Justin Stevens (jrsteven@jlab.org) or Mike Williams (mwill@mit.edu)

-----------------------------------------------------------------

Below are modifications to TMVA v4.1.2 needed to implement uBDT, for documentation purposes.  The original source before modifcation can be found at http://sourceforge.net/projects/tmva/files/TMVA/TMVA-v4.1.2/

Modifications to src/
-MethodPDERS.cxx: TMath::Power() with UInt_t gave compile errors so cast as Int_t (unrelated to UBDT but needed to compile with root v5.34.9)
-MethodCompositeBase.cxx: add flag to parse options specific to UBDT
-ModulekNN.cxx: add spectator variables to kNN::Event to store event ID
-MethodBDT.cxx: add variables and functions needed for uBoost and friend class to MethodUBDT
-MethodUBDT.cxx: add new class method to perform UBDT

Modifications to inc/TMVA/
-Types.h: add type kUBDT
-ModulekNN.h: add spectator variables to kNN::Event to store event ID
-MethodBDT.h: add variables and functions needed for uBoost
-MethodCategory.h: add MethodUBDT as friend class similar to MethodBoost
-MethodUBDT.h: add new class method to perform UBDT

Other changes:
Makefile: add MethodUBDT.h
inc/LinkDef1.h: add #pragma link C++ class TMVA::MethodUBDT+;
