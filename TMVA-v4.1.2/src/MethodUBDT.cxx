// @(#)root/tmva $Id$
// Author: Andreas Hoecker, Joerg Stelzer, Helge Voss, Kai Voss,Or Cohen, Jan Therhaag, Eckhard von Toerne

/**********************************************************************************
 * Project: TMVA - a Root-integrated toolkit for multivariate data analysis       *
 * Package: TMVA                                                                  *
 * Class  : MethodCompositeBase                                                   *
 * Web    : http://tmva.sourceforge.net                                           *
 *                                                                                *
 * Description:                                                                   *
 *      Virtual base class for all MVA method                                     *
 *                                                                                *
 * Authors (alphabetical):                                                        *
 *      Andreas Hoecker    <Andreas.Hocker@cern.ch>   - CERN, Switzerland         *
 *      Peter Speckmayer   <Peter.Speckmazer@cern.ch> - CERN, Switzerland         *
 *      Joerg Stelzer      <Joerg.Stelzer@cern.ch>    - CERN, Switzerland         *
 *      Helge Voss         <Helge.Voss@cern.ch>       - MPI-K Heidelberg, Germany *
 *      Jan Therhaag       <Jan.Therhaag@cern.ch>     - U of Bonn, Germany        *
 *      Eckhard v. Toerne  <evt@uni-bonn.de>          - U of Bonn, Germany        *
 *                                                                                *
 * Copyright (c) 2005-2011:                                                       *
 *      CERN, Switzerland                                                         *
 *      U. of Victoria, Canada                                                    *
 *      MPI-K Heidelberg, Germany                                                 *
 *      U. of Bonn, Germany                                                       *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in LICENSE           *
 * (http://tmva.sourceforge.net/LICENSE)                                          *
 **********************************************************************************/

//_______________________________________________________________________
//
// This class is meant to boost a single classifier. Boosting means    //
// training the classifier a few times. Everytime the wieghts of the   //
// events are modified according to how well the classifier performed  //
// on the test sample.                                                 //
//_______________________________________________________________________
#include <algorithm>
#include <iomanip>
#include <vector>
#include <cmath>

#include "Riostream.h"
#include "TRandom3.h"
#include "TMath.h"
#include "TObjString.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TSpline.h"
#include "TDirectory.h"

#include "TMVA/MethodCompositeBase.h"
#include "TMVA/MethodBase.h"
#include "TMVA/MethodBDT.h"
#include "TMVA/MethodUBDT.h"
#include "TMVA/MethodCategory.h"
#include "TMVA/Tools.h"
#include "TMVA/ClassifierFactory.h"
#include "TMVA/Timer.h"
#include "TMVA/Types.h"
#include "TMVA/PDF.h"
#include "TMVA/Results.h"
#include "TMVA/Config.h"

#include "TMVA/SeparationBase.h"
#include "TMVA/MisClassificationError.h"
#include "TMVA/GiniIndex.h"
#include "TMVA/CrossEntropy.h"
#include "TMVA/RegressionVariance.h"

REGISTER_METHOD(UBDT)

ClassImp(TMVA::MethodUBDT)

//_______________________________________________________________________
TMVA::MethodUBDT::MethodUBDT( const TString& jobName,
                                const TString& methodTitle,
                                DataSetInfo& theData,
                                const TString& theOption,
                                TDirectory* theTargetDir ) :
   TMVA::MethodCompositeBase( jobName, Types::kUBDT, methodTitle, theData, theOption, theTargetDir )
   , fUBDTNum(0)
   , fMinEffic(0)
   , fMaxEffic(0)
   , fnkNN(0)
   , fBalanceDepth(0)
   , fModule(0)
   , fUBDTMethodTitle(methodTitle)
   , fUBDTMethodOptions(theOption)
   , fMonitorUBDTMethod(kFALSE)
   , fMVAvalues(0)
   , fScaleFrac(0)
{
   fMVAvalues = new std::vector<Float_t>;
}

//_______________________________________________________________________
TMVA::MethodUBDT::MethodUBDT( DataSetInfo& dsi,
                                const TString& theWeightFile,
                                TDirectory* theTargetDir )
   : TMVA::MethodCompositeBase( Types::kUBDT, dsi, theWeightFile, theTargetDir )
   , fUBDTNum(0)
   , fMinEffic(0)
   , fMaxEffic(0)
   , fnkNN(0)
   , fBalanceDepth(0)
   , fModule(0)
   , fUBDTMethodTitle("")
   , fUBDTMethodOptions("")
   , fMonitorUBDTMethod(kFALSE)
   , fMVAvalues(0)
   , fScaleFrac(0)
{
   fMVAvalues = new std::vector<Float_t>;
}

//_______________________________________________________________________
TMVA::MethodUBDT::~MethodUBDT( void )
{
   // destructor
   fMethodWeight.clear();

   // the histogram themselves are deleted when the file is closed
   fTrainSigMVAHist.clear();
   fTrainBgdMVAHist.clear();
   fBTrainSigMVAHist.clear();
   fBTrainBgdMVAHist.clear();
   fTestSigMVAHist.clear();
   fTestBgdMVAHist.clear();
   fNLeavesHist.clear();
   fNLeavesByTreeHist.clear();
   
   if (fMVAvalues) {
      delete fMVAvalues;
      fMVAvalues = 0;
   }
   if (fModule) delete fModule;
}

//_______________________________________________________________________
Bool_t TMVA::MethodUBDT::HasAnalysisType( Types::EAnalysisType type, UInt_t numberClasses, UInt_t /*numberTargets*/ )
{
   // Just classification for now
   if (type == Types::kClassification && numberClasses == 2) return kTRUE;
   return kFALSE;
}


//_______________________________________________________________________
void TMVA::MethodUBDT::DeclareOptions()
{
   DeclareOptionRef( fUBDTNum = 1, "UBDT_Num",
                     "Number of steps in efficiency for UBDT" );
   DeclareOptionRef( fMinEffic = 0.0, "UBDT_MinEffic",
                     "Lowest efficiency step considered for UBDT training" );
   DeclareOptionRef( fMaxEffic = 1.0, "UBDT_MaxEffic",
                     "Highest efficiency step considered for UBDT training" );


   DeclareOptionRef( fnkNN = 100, "nkNN",
		     "Number of k-nearest neighbors" );
   DeclareOptionRef( fBalanceDepth = 6, "BalanceDepth",
		     "Binary tree balance depth");
   DeclareOptionRef( fScaleFrac = 0.80, "ScaleFrac",
		     "Fraction of events used to compute variable width");

   DeclareOptionRef( fMonitorUBDTMethod = kTRUE, "UBDT_MonitorMethod",
                     "Write monitoring histograms for UBDT classifier" );

   TMVA::MethodCompositeBase::fMethods.reserve(fUBDTNum);
}

//_______________________________________________________________________
Bool_t TMVA::MethodUBDT::BookMethod( Types::EMVA theMethod, TString methodTitle, TString theOption )
{
   // just registering the string from which the boosted classifier will be created
   fUBDTMethodName     = Types::Instance().GetMethodName( theMethod );
   fUBDTMethodTitle    = methodTitle;
   fUBDTMethodOptions  = theOption;
   TString opts=theOption;
   opts.ToLower();

   return kTRUE;
}

//_______________________________________________________________________
void TMVA::MethodUBDT::Init()
{ 

  fModule = new kNN::ModulekNN();

}


//_______________________________________________________________________
void TMVA::MethodUBDT::CheckSetup()
{

   Log() << kDEBUG << "CheckSetup: fUBDTNum="<<fUBDTNum<< Endl;
   Log() << kDEBUG << "CheckSetup: fMinEffic="<<fMinEffic<<" fMaxEffic="<<fMaxEffic<< Endl;
   Log() << kDEBUG << "CheckSetup: fTrainSigMVAHist.size()="<<fTrainSigMVAHist.size()<<Endl;
   Log() << kDEBUG << "CheckSetup: fTestSigMVAHist.size()="<<fTestSigMVAHist.size()<<Endl;
   Log() << kDEBUG << "CheckSetup: fMonitorUBDTMethod=" << (fMonitorUBDTMethod? "true" : "false") << Endl;
   Log() << kDEBUG << "CheckSetup: MName=" << fUBDTMethodName << " Title="<< fUBDTMethodTitle<< Endl;
   Log() << kDEBUG << "CheckSetup: MOptions="<< fUBDTMethodOptions << Endl;
   Log() << kDEBUG << "CheckSetup: fMethodIndex=" <<fMethodIndex << Endl;
   if (fMethods.size()>0) Log() << kDEBUG << "CheckSetup: fMethods[0]" <<fMethods[0]<<Endl;
   Log() << kDEBUG << "CheckSetup: fMethodWeight.size()" << fMethodWeight.size() << Endl;
   if (fMethodWeight.size()>0) Log() << kDEBUG << "CheckSetup: fMethodWeight[0]="<<fMethodWeight[0]<<Endl;
   Log() << kDEBUG << "CheckSetup: trying to repair things" << Endl;

}


//_______________________________________________________________________
void TMVA::MethodUBDT::MakeKNN( void )
{

   Log() << "Building tree of nearest neighboor events ... patience please" << Endl;

   fModule->Clear();

   // first loop over the training events to build binary search tree
   for (UInt_t ievt = 0; ievt < GetNEvents(); ++ievt) {
      const Event*   evt_   = GetTrainingEvent(ievt);

      // only use signal events
      if (!DataInfo().IsSignal(evt_)) continue;

      if (evt_->GetNSpectators() < 1)
	Log() << kFATAL << "MethodUBDT::MakeKNN() - No spectator variables given to build kNN vector" << Endl;
      
      // choose spectator variables to use in NN calculation
      const UInt_t nvec = evt_->GetNSpectators();
      kNN::VarVec vvec(nvec, 0.0);
      for (UInt_t ivar = 0; ivar < nvec; ++ivar) {
	vvec[ivar] = evt_->GetSpectator(ivar);
      }
      // store event index in kNN event spectator variable
      kNN::VarVec svec(1, 0.0);
      svec[0] = ievt; 

      Double_t  weight = evt_->GetWeight();
      Short_t   event_type = 1; //signal

      // create event and add to tree for NN search
      kNN::Event event_knn(vvec, weight, event_type);
      event_knn.SetSpectators(svec);
      event_knn.SetTargets(evt_->GetTargets());
      fModule->Add(event_knn);
   }
   
   std::string option;
   fModule->Fill(static_cast<UInt_t>(fBalanceDepth),
                 static_cast<UInt_t>(100.0*fScaleFrac),
                 option);

   // second loop over the training events to get nearest neighbors
   for (UInt_t ievt = 0; ievt < GetNEvents(); ++ievt) {
      const Event*   evt_   = GetTrainingEvent(ievt);

      // only use signal events
      if (!DataInfo().IsSignal(evt_)) continue;
      
      fnkNN = 100;
      const Int_t nvar = evt_->GetNSpectators();
      const Double_t weight = evt_->GetWeight();
      const UInt_t knn = static_cast<UInt_t>(fnkNN);
      kNN::VarVec vvec(static_cast<UInt_t>(nvar), 0.0);

      for (Int_t ivar = 0; ivar < nvar; ++ivar) {
	vvec[ivar] = evt_->GetSpectator(ivar);
      }
      
      const kNN::Event event_knn(vvec, weight, 1);
      fModule->Find(event_knn, knn + 1); // +1 since query event will be in NN list

      vector<UInt_t> singleEvent;

      // request list of NN
      const kNN::List &rlist = fModule->GetkNNList();
      if (rlist.size() != knn + 1) {
	Log() << kFATAL << "kNN result list is empty" << Endl;
      }

      // loop on NN list
      for (kNN::List::const_iterator lit = rlist.begin(); lit != rlist.end(); ++lit) {
	const kNN::Node<kNN::Event> &node = *(lit->first);
	
	// skip same event 
	if( ievt == node.GetEvent().GetSpec(0) ) continue;
	
	// check NN matching
	//if(ievt == 1000) Log() << "neighboor event "<< node.GetEvent().GetSpec(0) << " " << node.GetEvent().GetVar(0) << " " << node.GetEvent().GetVar(1) << Endl;
	
	// fill vector of NN here
	singleEvent.push_back(node.GetEvent().GetSpec(0));
      }

      fUBoostNN.push_back(singleEvent);
   }
}


//_______________________________________________________________________
void TMVA::MethodUBDT::Train()
{
   TDirectory* methodDir( 0 );
   TString     dirName,dirTitle;

   if (Data()->GetNTrainingEvents()==0) Log() << kFATAL << "<Train> Data() has zero events" << Endl;
   Data()->SetCurrentType(Types::kTraining);

   // initialize kNN vector
   MakeKNN();

   if (fMethods.size() > 0) fMethods.clear();
   fMVAvalues->resize(Data()->GetNTrainingEvents(), 0.0);

   Log() << kINFO << "Training "<< fUBDTNum << " " << fUBDTMethodName << " for efficiency from " << fMinEffic*100 << "-" << fMaxEffic*100 << "% with title " << fUBDTMethodTitle << " Classifiers and kNN = " << fnkNN << "... patience please" << Endl;
   Timer timer( fUBDTNum, GetName() );

   // clean boosted method options
   CleanBoostOptions();

   // remove transformations for individual boosting steps
   // the transformation of the main method will be rerouted to each of the boost steps
   Ssiz_t varTrafoStart=fUBDTMethodOptions.Index("~VarTransform=");
   if (varTrafoStart >0) {
      Ssiz_t varTrafoEnd  =fUBDTMethodOptions.Index(":",varTrafoStart);
      if (varTrafoEnd<varTrafoStart)
	 varTrafoEnd=fUBDTMethodOptions.Length();
      fUBDTMethodOptions.Remove(varTrafoStart,varTrafoEnd-varTrafoStart);
   }

   // set efficiency points to train uBoost
   fEfficiencyStepSize = (fMaxEffic-fMinEffic)/(fUBDTNum);
   Float_t targetEfficiency = fMinEffic;

   //
   // training the classifiers
   for (fMethodIndex=0;fMethodIndex<fUBDTNum;fMethodIndex++) {

      // set boost weights to 1
      ResetBoostWeights();

      // set different uBoostTargetEffic for each method (ie. each BDT forest in UBDT)
      targetEfficiency += fEfficiencyStepSize;
      TString singleBDTMethodOptions = fUBDTMethodOptions; 
      singleBDTMethodOptions += ":uBoostTargetEffic=";
      singleBDTMethodOptions += targetEfficiency;
      //cout<<singleBDTMethodOptions<<endl;

      // the first classifier shows the option string output, the rest not
      if (fMethodIndex>0) TMVA::MsgLogger::InhibitOutput();

      IMethod* method = ClassifierFactory::Instance().Create(std::string(fUBDTMethodName),
                                                             GetJobName(),
                                                             Form("%s%03i", fUBDTMethodTitle.Data(),fMethodIndex),
                                                             DataInfo(),
                                                             singleBDTMethodOptions);
      TMVA::MsgLogger::EnableOutput();

      // supressing the rest of the classifier output the right way
      MethodBase *meth = (dynamic_cast<MethodBase*>(method));

      if (meth==0) continue;

      // set fDataSetManager if MethodCategory (to enable Category to create datasetinfo objects) // DSMTEST
      if (meth->GetMethodType() == Types::kCategory) { // DSMTEST
         MethodCategory *methCat = (dynamic_cast<MethodCategory*>(meth)); // DSMTEST
         if (!methCat) // DSMTEST
            Log() << kFATAL << "Method with type kCategory cannot be casted to MethodCategory. /MethodUBDT" << Endl; // DSMTEST
         methCat->fDataSetManager = fDataSetManager; // DSMTEST
      } // DSMTEST

      meth->SetMsgType(kWARNING);
      meth->SetupMethod();
      meth->ParseOptions();
      // put SetAnalysisType here for the needs of MLP
      meth->SetAnalysisType( GetAnalysisType() );
      meth->ProcessSetup();
      meth->CheckSetup();

      // reroute transformationhandler
      meth->RerouteTransformationHandler (&(this->GetTransformationHandler()));

      // creating the directory of the classifier
      if (fMonitorUBDTMethod) {
         methodDir=MethodBaseDir()->GetDirectory(dirName=Form("%s_B%03i",fUBDTMethodName.Data(),fMethodIndex));
         if (methodDir==0) {
            methodDir=BaseDir()->mkdir(dirName,dirTitle=Form("Directory %s #%03i", fUBDTMethodName.Data(),fMethodIndex));
         }
         MethodBase* m = dynamic_cast<MethodBase*>(method);
         if (m) {
            m->SetMethodDir(methodDir);
            m->BaseDir()->cd();
         }
      }

      // training
      TMVA::MethodCompositeBase::fMethods.push_back(method);
      timer.DrawProgressBar( fMethodIndex );
      TMVA::MsgLogger::InhibitOutput(); //supressing Logger outside the method
      
      // set NN vector to use in training individual BDT forest
      MethodBDT* methodBDT = dynamic_cast<MethodBDT*>(method);
      for(UInt_t i=0; i<GetNEvents(); i++){
	const Event*   evt_   = GetTrainingEvent(i);
	if (!DataInfo().IsSignal(evt_)) continue;
	vector<UInt_t> singleEvent;
	for(UInt_t j=0; j<fUBoostNN[i].size(); j++){
	  singleEvent.push_back(fUBoostNN[i].at(j));
	}
	methodBDT->fUBoostNN.push_back(singleEvent);
      } 

      SingleTrain();
      TMVA::MsgLogger::EnableOutput();
      method->WriteMonitoringHistosToFile();
      //TestClassification();
      //WriteEvaluationHistosToFile(Types::kTraining);

      // calculate MVA values of method on training sample
      CalcMVAValues();

      // find the MVA cut for target efficiency of this classifier
      FindMVACut(targetEfficiency);

      // monitoring histograms for composite method
      if (fMethodIndex==0 && fMonitorUBDTMethod) CreateMVAHistorgrams();
      
      // fill monitoring histograms for single classifier
      if (fMonitorUBDTMethod) SingleFillHistograms();

      fMethodWeight.push_back(1.0); //needed by xml writer

      methodBDT->fUBoostNN.clear();
   }
   ResetBoostWeights();
}

//_______________________________________________________________________
void TMVA::MethodUBDT::CleanBoostOptions()
{
   fUBDTMethodOptions=GetOptions(); 
}

//_______________________________________________________________________
void TMVA::MethodUBDT::CreateMVAHistorgrams()
{
   if (fUBDTNum <=0) Log() << kFATAL << "CreateHistorgrams called before fUBDTNum is initialized" << Endl;
   // calculating histograms boundries and creating histograms..
   // nrms = number of rms around the average to use for outline (of the 0 classifier)
   Double_t meanS, meanB, rmsS, rmsB, xmin, xmax, nrms = 10;
   Int_t signalClass = 0;
   if (DataInfo().GetClassInfo("Signal") != 0) {
      signalClass = DataInfo().GetClassInfo("Signal")->GetNumber();
   }
   gTools().ComputeStat( GetEventCollection( Types::kMaxTreeType ), fMVAvalues,
                         meanS, meanB, rmsS, rmsB, xmin, xmax, signalClass );

   fNbins = gConfig().fVariablePlotting.fNbinsXOfROCCurve;
   xmin = TMath::Max( TMath::Min(meanS - nrms*rmsS, meanB - nrms*rmsB ), xmin );
   xmax = TMath::Min( TMath::Max(meanS + nrms*rmsS, meanB + nrms*rmsB ), xmax ) + 0.00001;

   // creating all the historgrams
   for (Int_t imtd=0; imtd<fUBDTNum; imtd++) {
      fTrainSigMVAHist .push_back( new TH1F( Form("MVA_Train_S%03i",imtd), "MVA_Train_S",        fNbins, xmin, xmax ) );
      fTrainBgdMVAHist .push_back( new TH1F( Form("MVA_Train_B%03i", imtd), "MVA_Train_B",        fNbins, xmin, xmax ) );
      fBTrainSigMVAHist.push_back( new TH1F( Form("MVA_BTrain_S%03i",imtd), "MVA_BoostedTrain_S", fNbins, xmin, xmax ) );
      fBTrainBgdMVAHist.push_back( new TH1F( Form("MVA_BTrain_B%03i",imtd), "MVA_BoostedTrain_B", fNbins, xmin, xmax ) );
      fTestSigMVAHist  .push_back( new TH1F( Form("MVA_Test_S%03i",  imtd), "MVA_Test_S",         fNbins, xmin, xmax ) );
      fTestBgdMVAHist  .push_back( new TH1F( Form("MVA_Test_B%03i",  imtd), "MVA_Test_B",         fNbins, xmin, xmax ) );
      fNLeavesHist     .push_back( new TH1F( Form("NLeaves%03i",  imtd), "NLeaves",         100, 0., 20. ) );
      fNLeavesByTreeHist.push_back( new TH1F( Form("NLeavesByTree%03i",  imtd), "NLeaves by Tree",         100, 0., 100. ) );
   }
}

//_______________________________________________________________________
void TMVA::MethodUBDT::ResetBoostWeights()
{
   // resetting back the boosted weights of the events to 1
   for (Long64_t ievt=0; ievt<GetNEvents(); ievt++) {
      Event *ev = Data()->GetEvent(ievt);
      ev->SetBoostWeight( 1.0 );
   }
}

//_______________________________________________________________________
void TMVA::MethodUBDT::WriteMonitoringHistosToFile( void ) const
{
   TDirectory* dir=0;
   if (fMonitorUBDTMethod) {
      for (Int_t imtd=0;imtd<fUBDTNum;imtd++) {

         //writing the histograms in the specific classifier's directory
         MethodBase* m = dynamic_cast<MethodBase*>(fMethods[imtd]);
         if (!m) continue;
         dir = m->BaseDir();
         dir->cd();
         fTrainSigMVAHist[imtd]->SetDirectory(dir);
         fTrainSigMVAHist[imtd]->Write();
         fTrainBgdMVAHist[imtd]->SetDirectory(dir);
         fTrainBgdMVAHist[imtd]->Write();
         fBTrainSigMVAHist[imtd]->SetDirectory(dir);
         fBTrainSigMVAHist[imtd]->Write();
         fBTrainBgdMVAHist[imtd]->SetDirectory(dir);
         fBTrainBgdMVAHist[imtd]->Write();
	 fNLeavesHist[imtd]->SetDirectory(dir);
	 fNLeavesHist[imtd]->Write();
	 fNLeavesByTreeHist[imtd]->SetDirectory(dir);
	 fNLeavesByTreeHist[imtd]->Write();
      }
   }
}

//_______________________________________________________________________
void TMVA::MethodUBDT::TestClassification()
{
   MethodBase::TestClassification();
   if (fMonitorUBDTMethod) {
      UInt_t nloop = fTestSigMVAHist.size();
      if (fMethods.size()<nloop) nloop = fMethods.size();
      //running over all the events and populating the test MVA histograms
      Data()->SetCurrentType(Types::kTesting);
      for (Long64_t ievt=0; ievt<GetNEvents(); ievt++) {
         const Event* ev = GetEvent(ievt);
         Float_t w = ev->GetWeight();
         if (DataInfo().IsSignal(ev)) {
            for (UInt_t imtd=0; imtd<nloop; imtd++) {
               fTestSigMVAHist[imtd]->Fill(fMethods[imtd]->GetMvaValue(),w);
            }
         }
         else {
            for (UInt_t imtd=0; imtd<nloop; imtd++) {
               fTestBgdMVAHist[imtd]->Fill(fMethods[imtd]->GetMvaValue(),w);
            }
         }
      }
      Data()->SetCurrentType(Types::kTraining);
   }
}

//_______________________________________________________________________
void TMVA::MethodUBDT::WriteEvaluationHistosToFile(Types::ETreeType treetype)
{
   MethodBase::WriteEvaluationHistosToFile(treetype);
   if (treetype==Types::kTraining) return;
   UInt_t nloop = fTestSigMVAHist.size();
   if (fMethods.size()<nloop) nloop = fMethods.size();
   if (fMonitorUBDTMethod) {
      TDirectory* dir=0;
      for (UInt_t imtd=0;imtd<nloop;imtd++) {
         //writing the histograms in the specific classifier's directory
         MethodBase* mva = dynamic_cast<MethodBase*>(fMethods[imtd]);
         if (!mva) continue;
         dir = mva->BaseDir();
         if (dir==0) continue;
         dir->cd();
         fTestSigMVAHist[imtd]->SetDirectory(dir);
         fTestSigMVAHist[imtd]->Write();
         fTestBgdMVAHist[imtd]->SetDirectory(dir);
         fTestBgdMVAHist[imtd]->Write();
      }
   }
}

//_______________________________________________________________________
void TMVA::MethodUBDT::ProcessOptions()
{

   // process the options specified by the user
   if (!(fnkNN > 0)) {
      fnkNN = 10;
      Log() << kWARNING << "kNN must be a positive integer: set kNN = " << fnkNN << Endl;
   }
#if 0
   if (fScaleFrac < 0.0) {
      fScaleFrac = 0.0;
      Log() << kWARNING << "ScaleFrac can not be negative: set ScaleFrac = " << fScaleFrac << Endl;
   }
   if (fScaleFrac > 1.0) {
      fScaleFrac = 1.0;
   }
#endif
   if (!(fBalanceDepth > 0)) {
      fBalanceDepth = 6;
      Log() << kWARNING << "Optimize must be a positive integer: set Optimize = " << fBalanceDepth << Endl;
   }

   Log() << kVERBOSE
         << "kNN options: \n"
         << "  kNN = \n" << fnkNN
     //<< "  ScaleFrac = \n" << fScaleFrac
         << "  Optimize = " << fBalanceDepth << Endl;

}

//_______________________________________________________________________
void TMVA::MethodUBDT::SingleTrain()
{
   // initialization
   Data()->SetCurrentType(Types::kTraining);
   MethodBase* meth = dynamic_cast<MethodBase*>(GetLastMethod());
   if (meth) meth->TrainMethod();
}

//_______________________________________________________________________
void TMVA::MethodUBDT::SingleFillHistograms()
{
   MethodBase* method =  dynamic_cast<MethodBase*>(fMethods.back());
   if (!method) return;
   Float_t w,v,wo; Bool_t sig=kTRUE;

   // finding the wrong events and calculating their total weights
   for (Long64_t ievt=0; ievt<GetNEvents(); ievt++) {
      const Event* ev = GetTrainingEvent(ievt);
      sig=DataInfo().IsSignal(ev);
      v = fMVAvalues->at(ievt);
      w = ev->GetWeight();
      wo = ev->GetOriginalWeight();
      if (sig && fMonitorUBDTMethod) {
         fBTrainSigMVAHist[fMethodIndex]->Fill(v,w);
         fTrainSigMVAHist[fMethodIndex]->Fill(v,ev->GetOriginalWeight());
      }
      else if (fMonitorUBDTMethod) {
         fBTrainBgdMVAHist[fMethodIndex]->Fill(v,w);
         fTrainBgdMVAHist[fMethodIndex]->Fill(v,ev->GetOriginalWeight());
      }
   }

   MethodBDT* methodBDT =  dynamic_cast<MethodBDT*>(fMethods.back());
   if (!methodBDT) return;
   const vector<TMVA::DecisionTree*> Forest = methodBDT->GetForest();
   for (UInt_t itree=0; itree<Forest.size(); itree++){
     fNLeavesHist[fMethodIndex]->Fill(Forest[itree]->CountLeafNodes(NULL));
     fNLeavesByTreeHist[fMethodIndex]->SetBinContent(itree+1,Forest[itree]->CountLeafNodes(NULL));
   }
}

//_______________________________________________________________________
void TMVA::MethodUBDT::GetHelpMessage() const
{
   // Get help message text
   //
   // typical length of text line:
   //         "|--------------------------------------------------------------|"
   Log() << Endl;
   Log() << gTools().Color("bold") << "--- Short description:" << gTools().Color("reset") << Endl;
   Log() << Endl;
   
}

//_______________________________________________________________________
const TMVA::Ranking* TMVA::MethodUBDT::CreateRanking()
{ 
   return 0;
}

//_______________________________________________________________________
Double_t TMVA::MethodUBDT::GetMvaValue( Double_t* err, Double_t* errUpper )
{
  // return boosted MVA response
   Double_t mvaValue = 0;
   for (UInt_t i=0;i< fMethods.size(); i++){
      MethodBase* m = dynamic_cast<MethodBase*>(fMethods[i]);
      if (m==0) continue;
      Double_t val = fTmpEvent ? m->GetMvaValue(fTmpEvent) : m->GetMvaValue();

      if(val > m->GetSignalReferenceCut()) {
	mvaValue+=1./(int)fMethods.size(); 
      }
   }

   // cannot determine error
   NoErrorCalc(err, errUpper);

   return mvaValue;
}

void TMVA::MethodUBDT::CalcMVAValues()
{
   // Calculate MVA values of current method fMethods.back() on
   // training sample

   Data()->SetCurrentType(Types::kTraining);
   MethodBase* method = dynamic_cast<MethodBase*>(fMethods.back());
   if (!method) {
      Log() << kFATAL << "dynamic cast to MethodBase* failed" <<Endl;
      return;
   }
   // calculate MVA values
   for (Long64_t ievt=0; ievt<GetNEvents(); ievt++) {
      GetEvent(ievt);
      fMVAvalues->at(ievt) = method->GetMvaValue();
   }
}

//_______________________________________________________________________
void TMVA::MethodUBDT::FindMVACut(Float_t targetEfficiency)
{

   // find the CUT on the individual MVA that defines an event as
   // correct or misclassified (to be used in the boosting process)
 
   MethodBase* method = dynamic_cast<MethodBase*>(fMethods.back());
   if (!method) {
      Log() << kFATAL << "dynamic cast to MethodBase* failed" <<Endl;
      return;
   }
  
   // get signal MVA values
   std::vector<Float_t> *fMVAvaluesSignal = new std::vector<Float_t>;
   Data()->SetCurrentType(Types::kTraining);
   for (Long64_t ievt=0; ievt<GetNEvents(); ievt++) {
     if(!DataInfo().IsSignal(GetEvent(ievt))) continue;
     fMVAvaluesSignal->push_back(fMVAvalues->at(ievt));
   }

   // set signal reference cut
   sort(fMVAvaluesSignal->begin(),fMVAvaluesSignal->end());
   Float_t sumSignal = 0.; 
   Float_t minBDT = 1.0;
   for(vector<Float_t>::iterator it = fMVAvaluesSignal->end(); it!=fMVAvaluesSignal->begin(); it--){
     sumSignal++;
     if(sumSignal/(Float_t)fMVAvaluesSignal->size() >= targetEfficiency) {
       minBDT = *it; break;
     }
   }
   method->SetSignalReferenceCut(minBDT);
   
   delete fMVAvaluesSignal;
   
   return;
}
